#!/usr/bin/env fennel
(local utils {:tcat table.concat})
(local tcat utils.tcat)



(lambda utils.any->string [o ?sep]
  "turns tables into strings using table.concat,
otherwise use the tostring builtin.
takes an optional separator variable as the second argument,
which is only used in the context of tables, and defaults to ' '
"
  (let [sep (or ?sep " ")]
	(match (type o)
	  :table	(tcat o sep)
	  :string	o
	  :number	(tostring o)
	  _ (tostring 0))))




(λ utils.exec [command ?sep]
  "execute:
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings."
  (os.execute (utils.any->string command ?sep)))

;;(lambda utils.mkdir [])



utils
