#!/usr/bin/env fennel
(local f (require :file))
(local tcat table.concat)
(local lines
	   ["QT_QPA_PLATFORMTHEME=qt5ct"
		"QT_STYLE_OVERRIDE=kvantum"
		"EDITOR=nvim"
		"BROWSER=vivaldi"])
(local envpath "airootfs/etc/environment")
(os.execute (tcat ["touch" envpath] " "))
(local file (f.init {:name envpath}))
(icollect [lineno line (ipairs lines) &into file.lines] line)
(file:write!)



